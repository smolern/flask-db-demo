import os
import sys
from app import app
from flask import render_template
from flask import request
from .models import UserForm
from app import db

@app.route('/')
@app.route('/index.html')
@app.route('/index.php')
def index():
    user={'name':'Alex','lname':'Schapelle'}
    return render_template('index.html', title='Home', user=user)

@app.route('/signin',methods=['GET', 'POST'])
def signin():
    form = UserForm()
    if request.method == 'POST':
        num = 0
        user = request.args.get('username')
        mail = request.args.get('usermail')
        db.insert({'user_'+str(num + 1):user, 'usermail':mail})
    return render_template('signin.html', title='Leave E-mail', form=form)